import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rshb/gateway/product/product.dart';
import 'package:rshb/gateway/product/stub_product.dart';
import 'package:rshb/storage/favourite/favourite.dart';
import 'package:rshb/storage/favourite/shared_preferences/shared_preferences_favourite.dart';
import 'package:rshb/view/product_list/product_list_screen.dart';
import 'package:shared_preferences/shared_preferences.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        Provider<ProductGateway>(
          create: (_) => StubProductGateway(),
        ),
        FutureProvider<SharedPreferences>(
          create: (_) => SharedPreferences.getInstance(),
        ),
        ChangeNotifierProxyProvider<SharedPreferences, FavouriteProductStorage>(
          create: (context) => SharedPreferencesFavouriteProductStorage(null),
          update: (context, value, previous) => SharedPreferencesFavouriteProductStorage(value)..init(),
        ),
      ],
      child: MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.blue,
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        home: ProductListScreen(),
      ),
    );
  }
}
