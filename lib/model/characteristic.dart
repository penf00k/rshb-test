import 'package:flutter/foundation.dart';
import 'package:json_annotation/json_annotation.dart' as j;

part 'characteristic.g.dart';

@j.JsonSerializable(includeIfNull: false)
class Characteristic {
  final String title;
  final String value;

  Characteristic({
    @required this.title,
    @required this.value,
  });

  factory Characteristic.fromJson(Map<String, dynamic> json) => _$CharacteristicFromJson(json);

  Map<String, dynamic> toJson() => _$CharacteristicToJson(this);
}
