import 'package:flutter/foundation.dart';
import 'package:json_annotation/json_annotation.dart' as j;

part 'section.g.dart';

@j.JsonSerializable(includeIfNull: false)
class Section {
  final int id;
  final String title;

  Section({
    @required this.id,
    @required this.title,
  });

  factory Section.fromJson(Map<String, dynamic> json) => _$SectionFromJson(json);

  Map<String, dynamic> toJson() => _$SectionToJson(this);
}
