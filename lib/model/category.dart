import 'package:flutter/foundation.dart';
import 'package:json_annotation/json_annotation.dart' as j;

part 'category.g.dart';

@j.JsonSerializable(includeIfNull: false)
class Category {
  final int id;
  final int sectionId;
  final String title;
  final String icon;

  Category({
    @required this.id,
    @required this.sectionId,
    @required this.title,
    @required this.icon,
  });

  factory Category.fromJson(Map<String, dynamic> json) => _$CategoryFromJson(json);

  Map<String, dynamic> toJson() => _$CategoryToJson(this);
}
