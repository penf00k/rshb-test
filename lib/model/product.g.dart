// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'product.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Product _$ProductFromJson(Map<String, dynamic> json) {
  return Product(
    id: json['id'] as int,
    sectionId: json['sectionId'] as int,
    categoryId: json['categoryId'] as int,
    farmerId: json['farmerId'] as int,
    title: json['title'] as String,
    unit: json['unit'] as String,
    totalRating: (json['totalRating'] as num)?.toDouble(),
    ratingCount: json['ratingCount'] as int,
    image: json['image'] as String,
    shortDescription: json['shortDescription'] as String,
    description: json['description'] as String,
    price: (json['price'] as num)?.toDouble(),
    characteristics: (json['characteristics'] as List)
        ?.map((e) => e == null
            ? null
            : Characteristic.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$ProductToJson(Product instance) {
  final val = <String, dynamic>{};

  void writeNotNull(String key, dynamic value) {
    if (value != null) {
      val[key] = value;
    }
  }

  writeNotNull('id', instance.id);
  writeNotNull('sectionId', instance.sectionId);
  writeNotNull('categoryId', instance.categoryId);
  writeNotNull('farmerId', instance.farmerId);
  writeNotNull('title', instance.title);
  writeNotNull('unit', instance.unit);
  writeNotNull('totalRating', instance.totalRating);
  writeNotNull('ratingCount', instance.ratingCount);
  writeNotNull('image', instance.image);
  writeNotNull('shortDescription', instance.shortDescription);
  writeNotNull('description', instance.description);
  writeNotNull('price', instance.price);
  writeNotNull('characteristics', instance.characteristics);
  return val;
}
