import 'package:flutter/foundation.dart';
import 'package:json_annotation/json_annotation.dart' as j;

part 'farmer.g.dart';

@j.JsonSerializable(includeIfNull: false)
class Farmer {
  final int id;
  final String title;

  Farmer({
    @required this.id,
    @required this.title,
  });

  factory Farmer.fromJson(Map<String, dynamic> json) => _$FarmerFromJson(json);

  Map<String, dynamic> toJson() => _$FarmerToJson(this);
}
