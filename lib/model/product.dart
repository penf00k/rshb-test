import 'package:flutter/foundation.dart';
import 'package:json_annotation/json_annotation.dart' as j;

import 'characteristic.dart';

part 'product.g.dart';

@j.JsonSerializable(includeIfNull: false)
class Product {
  final int id;
  final int sectionId;
  final int categoryId;
  final int farmerId;
  final String title;
  final String unit;
  final double totalRating;
  final int ratingCount;
  final String image;
  final String shortDescription;
  final String description;
  final double price;
  final List<Characteristic> characteristics;

  Product({
    @required this.id,
    @required this.sectionId,
    @required this.categoryId,
    @required this.farmerId,
    @required this.title,
    @required this.unit,
    @required this.totalRating,
    @required this.ratingCount,
    @required this.image,
    @required this.shortDescription,
    @required this.description,
    @required this.price,
    @required this.characteristics,
  });

  factory Product.fromJson(Map<String, dynamic> json) => _$ProductFromJson(json);

  Map<String, dynamic> toJson() => _$ProductToJson(this);
}
