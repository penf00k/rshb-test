import 'package:flutter/material.dart';
import 'package:json_annotation/json_annotation.dart' as j;

import 'category.dart';
import 'farmer.dart';
import 'product.dart';
import 'section.dart';

part 'response.g.dart';

@j.JsonSerializable(includeIfNull: false)
class ProductResponse {
  final List<Section> sections;
  final List<Category> categories;
  final List<Product> products;
  final List<Farmer> farmers;
  final Map<int, Section> sectionsMap;
  final Map<int, Category> categoriesMap;
  final Map<int, Farmer> farmersMap;

  ProductResponse({
    @required this.sections,
    @required this.categories,
    @required this.products,
    @required this.farmers,
  })  : sectionsMap = Map<int, Section>.fromIterable(sections, key: (s) => s.id, value: (s) => s),
        categoriesMap = Map<int, Category>.fromIterable(categories, key: (s) => s.id, value: (s) => s),
        farmersMap = Map<int, Farmer>.fromIterable(farmers, key: (s) => s.id, value: (s) => s);

  factory ProductResponse.fromJson(Map<String, dynamic> json) => _$ProductResponseFromJson(json);

  Map<String, dynamic> toJson() => _$ProductResponseToJson(this);
}
