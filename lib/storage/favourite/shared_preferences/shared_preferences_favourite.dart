import 'dart:convert';

import 'package:rshb/storage/favourite/favourite.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'shared_preferences_model.dart';

const _favouriteSharedPreferencesKey = 'ru.penf00k.rsgb.favouriteSharedPreferencesKey';

class SharedPreferencesFavouriteProductStorage extends FavouriteProductStorage {
  final SharedPreferences prefs;
  Set<int> _favouriteIds = {};

  SharedPreferencesFavouriteProductStorage(this.prefs);

  @override
  Future<void> persistFavourite(Set<int> favouriteIds) async {
    final favouriteProducts = SharedPreferencesFavouriteProducts(favouriteIds.toList());
    await prefs.setString(_favouriteSharedPreferencesKey, jsonEncode(favouriteProducts.toJson()));
  }

  @override
  Set<int> get favouriteIds => _favouriteIds;

  Future<void> init() async {
    if (prefs == null) return;

    final json = prefs.getString(_favouriteSharedPreferencesKey);

    if (json == null) {
      _favouriteIds = {};
    } else {
      final map = jsonDecode(json);
      final favouriteProducts = SharedPreferencesFavouriteProducts.fromJson(map);
      _favouriteIds = Set<int>.from(favouriteProducts.productIds);
    }

    notifyListeners();
  }
}
