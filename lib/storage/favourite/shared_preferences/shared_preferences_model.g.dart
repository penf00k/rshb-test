// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'shared_preferences_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SharedPreferencesFavouriteProducts _$SharedPreferencesFavouriteProductsFromJson(
    Map<String, dynamic> json) {
  return SharedPreferencesFavouriteProducts(
    (json['productIds'] as List)?.map((e) => e as int)?.toList(),
  );
}

Map<String, dynamic> _$SharedPreferencesFavouriteProductsToJson(
        SharedPreferencesFavouriteProducts instance) =>
    <String, dynamic>{
      'productIds': instance.productIds,
    };
