import 'package:json_annotation/json_annotation.dart' as j;

part 'shared_preferences_model.g.dart';

@j.JsonSerializable()
class SharedPreferencesFavouriteProducts {
  final List<int> productIds;

  SharedPreferencesFavouriteProducts(this.productIds);

  factory SharedPreferencesFavouriteProducts.fromJson(Map<String, dynamic> json) =>
      _$SharedPreferencesFavouriteProductsFromJson(json);

  Map<String, dynamic> toJson() => _$SharedPreferencesFavouriteProductsToJson(this);
}
