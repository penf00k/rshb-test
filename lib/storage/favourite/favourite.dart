import 'package:flutter/cupertino.dart';

abstract class FavouriteProductStorage extends ChangeNotifier {
  Set<int> get favouriteIds;

  FavouriteProductStorage();

  Future<void> persistFavourite(Set<int> favouriteIds);

  Future<void> setFavourite(int productId, bool favourite) async {
    if (favourite)
      favouriteIds.add(productId);
    else
      favouriteIds.remove(productId);

    await persistFavourite(favouriteIds);

    notifyListeners();
  }
}
