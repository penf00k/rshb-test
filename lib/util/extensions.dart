extension ItearableExt<T> on Iterable<T> {
  List<R> mapExt<R>(R element(int index, T e), {R delimiter(int index)}) {
    final res = <R>[];
    for (int i = 0; i < length; i++) {
      final item = this.elementAt(i);
      res.add(element(i, item));
      if (delimiter != null && i < length - 1) res.add(delimiter(i));
    }
    return res;
  }
}
