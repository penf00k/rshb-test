import 'package:rshb/model/response.dart';

abstract class ProductGateway {
  Future<ProductResponse> getProductData();
}
