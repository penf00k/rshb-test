import 'dart:math';

import 'package:rshb/model/category.dart';
import 'package:rshb/model/characteristic.dart';
import 'package:rshb/model/farmer.dart';
import 'package:rshb/model/product.dart';
import 'package:rshb/model/response.dart';
import 'package:rshb/model/section.dart';

import 'product.dart';

const images = <String>[
  'https://cdn.pixabay.com/photo/2020/05/25/19/58/fox-5220328_960_720.jpg',
  'https://cdn.pixabay.com/photo/2020/07/18/11/46/old-man-5416999_960_720.jpg',
  'https://cdn.pixabay.com/photo/2020/06/29/17/32/snail-5353573_960_720.jpg',
  'https://cdn.pixabay.com/photo/2020/06/21/04/25/nyc-5323170_960_720.jpg',
];

class StubProductGateway extends ProductGateway {
  final Random rand = Random();

  @override
  Future<ProductResponse> getProductData() async {
    await _wait();

    return ProductResponse(
      sections: [
        Section(id: 1, title: 'Продукты'),
        Section(id: 2, title: 'Фермеры'),
        Section(id: 3, title: 'Агротуры'),
      ],
      categories: List.generate(
        10,
        (index) {
          final title = 'Категория $index ${index % 2 == 0 ? 'много букв' : ''}';

          return Category(
            id: index,
            sectionId: 1,
            title: title,
            icon: 'assets/vegetables.svg',
          );
        },
      ),
      farmers: [
        Farmer(id: 1, title: 'Молочный рай'),
        Farmer(id: 2, title: 'Мясное королевство'),
        Farmer(id: 3, title: 'Поросячье счастье')
      ],
      products: List.generate(
        15,
        (index) => Product(
          id: index,
          sectionId: 1,
          categoryId: index % 10 + 1,
          farmerId: index % 3 + 1,
          title: 'Молоко 5%',
          unit: '1л',
          totalRating: index * 0.3,
          ratingCount: index,
          image: images[index % images.length],
          shortDescription: 'Наше молоко и сливки поступают на завод в тот же день, когда доят коров.',
          description:
              'Наше молоко и сливки поступают на завод в тот же день, когда доят коров, и это свежее пастеризованное для обеспечения качества при сохранении его свежего фермерского вкуса и питательной ценности. Мы делаем это, чтобы ваша семья могла наслаждаться клеверным молоком и сливками с чистой совестью и хорошим здоровьем! Без гармона роста (rBST) \nи без антибиотиков — свежего пастеризованного для обеспечения качества — без глютена — \nс низким содержанием натрия.',
          price: index * 100.0,
          characteristics: List.generate(
            index,
            (index) => Characteristic(title: 'Вес продукта', value: '$index,680 кг'),
          ),
        ),
      ),
    );
  }

  Future _wait() async => await Future.delayed(Duration(milliseconds: 1300 + rand.nextInt(300)));
}
