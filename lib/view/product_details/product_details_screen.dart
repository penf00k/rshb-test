import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:provider/provider.dart';
import 'package:rshb/storage/favourite/favourite.dart';
import 'package:rshb/util/extensions.dart';
import 'package:rshb/view/expandable.dart';
import 'package:rshb/view/point_painter.dart';

const Radius _kRadius8 = Radius.circular(8);
const BorderRadius _kBorderRadius5 = BorderRadius.all(_kRadius8);
const Radius _kRadius20 = Radius.circular(20);
const int _kNotCollapsibleDetailsCount = 5;

class Pair<T1, T2> {
  final T1 first;
  final T2 second;

  Pair(this.first, this.second);
}

class ProductDetailsScreen extends StatelessWidget {
  final int id;
  final String imageUrl;
  final String title;
  final String unit;
  final double rating;
  final int ratingCount;
  final String description;
  final double price;
  final String heroTag;
  final List<Pair<String, String>> characteristics;

  const ProductDetailsScreen({
    Key key,
    @required this.id,
    @required this.imageUrl,
    @required this.title,
    @required this.unit,
    @required this.rating,
    @required this.ratingCount,
    @required this.description,
    @required this.price,
    @required this.heroTag,
    @required this.characteristics,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.of(context).size;
    final imageSize = screenSize.height / 3;

    return Consumer<FavouriteProductStorage>(
      builder: (context, favStorage, child) => Scaffold(
        body: CustomScrollView(
          slivers: <Widget>[
            SliverAppBar(
              expandedHeight: imageSize,
              backgroundColor: Colors.white,
              automaticallyImplyLeading: false,
              bottom: PreferredSize(
                child: Container(
                  height: 20.0,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                      topLeft: _kRadius20,
                      topRight: _kRadius20,
                    ),
                  ),
                ),
                preferredSize: Size.zero,
              ),
              title: _buildCollapsibleToolbar(context, favStorage),
              flexibleSpace: FlexibleSpaceBar(
                background: Hero(
                  tag: heroTag,
                  child: CachedNetworkImage(
                    imageUrl: imageUrl,
                    fit: BoxFit.cover,
                  ),
                ),
              ),
            ),
            SliverFillRemaining(
              hasScrollBody: false,
              child: _buildContent(),
            )
          ],
        ),
      ),
    );
  }

  Padding _buildCollapsibleToolbar(BuildContext context, FavouriteProductStorage favStorage) {
    final favourite = favStorage.favouriteIds.contains(id);

    return Padding(
      padding: const EdgeInsets.only(top: 16),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          IconButton(
            iconSize: 40,
            icon: Container(
              width: 40,
              height: 40,
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                border: Border.all(
                  color: Color(0x4c969696),
                  width: 1,
                ),
                color: Colors.white,
              ),
              child: Center(
                child: SvgPicture.asset('assets/arr_left.svg'),
              ),
            ),
            onPressed: () => Navigator.pop(context),
          ),
          GestureDetector(
            onTap: () {
              favStorage.setFavourite(id, !favourite);
            },
            child: Container(
              width: 40,
              height: 40,
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                border: Border.all(
                  color: Color(0xffbdbdbd),
                  style: favourite ? BorderStyle.none : BorderStyle.solid,
                ),
                color: Colors.white,
              ),
              child: Center(
                child: SvgPicture.asset(favourite ? 'assets/favourite_filled.svg' : 'assets/favourite_empty.svg'),
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget _buildContent() {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 16).copyWith(top: 10, bottom: 16),
      decoration: BoxDecoration(
        color: Colors.white,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          _buildTitle(),
          Padding(
            padding: const EdgeInsets.only(top: 12),
            child: _buildRating(),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 20),
            child: _buildPrice(),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 50),
            child: _buildDescription(),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 40),
            child: _ProductDetail(
              characteristics: characteristics,
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildTitle() {
    return RichText(
      textAlign: TextAlign.start,
      text: TextSpan(
        children: <TextSpan>[
          TextSpan(
            text: title,
            style: TextStyle(
              color: Color(0xff1c1c1c),
              fontSize: 16,
              fontFamily: 'SF Pro Display',
              fontWeight: FontWeight.w500,
            ),
          ),
          TextSpan(
            text: ' / $unit',
            style: TextStyle(
              color: Color(0xff969696),
              fontSize: 14,
              fontFamily: 'SF Pro Display',
              fontWeight: FontWeight.w400,
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildRating() {
    return Row(
      children: <Widget>[
        Container(
          width: 22,
          height: 22,
          decoration: BoxDecoration(
            color: Color(0xffd7a50c),
            borderRadius: _kBorderRadius5,
          ),
          child: Center(
            child: Text(
              rating.toStringAsFixed(1),
              textAlign: TextAlign.center,
              style: TextStyle(
                color: Color(0xffffffff),
                fontSize: 10,
                fontFamily: 'SF Pro Display',
                fontWeight: FontWeight.w600,
              ),
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 5),
          child: Text(
            '$ratingCount оценок',
            style: TextStyle(
              color: Color(0xff969696),
              fontSize: 10,
              fontFamily: 'SF Pro Display',
              fontWeight: FontWeight.w400,
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildPrice() {
    return Text(
      '${price.floor()} ₽',
      style: TextStyle(
        color: Color(0xff1c1c1c),
        fontSize: 14,
        fontFamily: 'SF Pro Display',
        fontWeight: FontWeight.w600,
      ),
    );
  }

  Widget _buildDescription() {
    return Text(
      description,
      style: TextStyle(
        color: Color(0xff1c1c1c),
        fontSize: 14,
        fontFamily: 'SF Pro Display',
        fontWeight: FontWeight.w400,
      ),
    );
  }
}

class _ProductDetail extends StatefulWidget {
  final List<Pair<String, String>> characteristics;

  const _ProductDetail({
    Key key,
    @required this.characteristics,
  }) : super(key: key);

  @override
  _ProductDetailState createState() => _ProductDetailState();
}

class _ProductDetailState extends State<_ProductDetail> with SingleTickerProviderStateMixin {
  bool _expanded = false;
  AnimationController rotationController;

  @override
  void initState() {
    super.initState();
    rotationController = AnimationController(
      vsync: this,
      value: 1.0,
      duration: Duration(milliseconds: 300),
      animationBehavior: AnimationBehavior.preserve,
    );
  }

  int get _clampedNotCollapsibleCount => _kNotCollapsibleDetailsCount.clamp(0, widget.characteristics.length);

  Iterable<Pair<String, String>> get _persistentCharacteristics =>
      widget.characteristics.getRange(0, _clampedNotCollapsibleCount);

  Iterable<Pair<String, String>> get _collapsibleCharacteristics =>
      widget.characteristics.getRange(_clampedNotCollapsibleCount, widget.characteristics.length);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        ..._persistentCharacteristics
            .mapExt(
              (i, s) => _buildDetailsItem(s.first, s.second),
              delimiter: (_) => SizedBox(height: 16),
            )
            .toList(),
        if (_expanded)
          Expandable(
            expand: _expanded,
            child: Padding(
              padding: const EdgeInsets.only(top: 16),
              child: Column(
                children: _collapsibleCharacteristics
                    .mapExt(
                      (i, s) => _buildDetailsItem(s.first, s.second),
                      delimiter: (_) => SizedBox(height: 16),
                    )
                    .toList(),
              ),
            ),
          ),
        if (widget.characteristics.length > _kNotCollapsibleDetailsCount) _buildButton(),
      ],
    );
  }

  FlatButton _buildButton() {
    return FlatButton(
      padding: const EdgeInsets.all(0),
      onPressed: () {
        setState(() {
          _expanded = !_expanded;
        });
        if (!_expanded) {
          rotationController.forward();
        } else {
          rotationController.reverse();
        }
      },
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Text(
            'Все характеристики',
            style: TextStyle(
              color: Color(0xff41ab43),
              fontSize: 14,
              fontFamily: 'SF Pro Display',
              fontWeight: FontWeight.w600,
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 8, right: 2),
            child: RotationTransition(
              turns: Tween(begin: 0.75, end: 1.0).animate(rotationController),
              child: SvgPicture.asset('assets/chevron_right.svg'),
            ),
          )
        ],
      ),
    );
  }

  Widget _buildDetailsItem(String title, String value) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          title,
          style: TextStyle(
            color: Color(0xff969696),
            fontSize: 12,
            fontFamily: 'SF Pro Display',
            fontWeight: FontWeight.w400,
          ),
        ),
        Expanded(
          child: CustomPaint(
            painter: PointPainter(color: Color(0xffDADADA)),
          ),
        ),
        Text(
          value,
          textAlign: TextAlign.right,
          style: TextStyle(
            color: Color(0xff1c1c1c),
            fontSize: 12,
            fontFamily: 'SF Pro Display',
            fontWeight: FontWeight.w500,
          ),
        )
      ],
    );
  }
}
