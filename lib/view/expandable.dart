import 'package:flutter/material.dart';

class Expandable extends StatefulWidget {
  final Widget child;
  final bool expand;

  Expandable({
    @required this.expand,
    @required this.child,
  });

  @override
  _ExpandableState createState() => _ExpandableState();
}

class _ExpandableState extends State<Expandable> with SingleTickerProviderStateMixin {
  AnimationController expandController;
  Animation<double> animation;

  @override
  void initState() {
    super.initState();
    prepareAnimations();
  }

  void prepareAnimations() {
    expandController = AnimationController(vsync: this, duration: Duration(milliseconds: 300));
    Animation curve = CurvedAnimation(
      parent: expandController,
      curve: Curves.fastOutSlowIn,
    );
    animation = Tween(begin: 0.0, end: 1.0).animate(curve)
      ..addListener(() {
        setState(() {});
      });
    if (widget.expand) expandController.forward();
  }

  @override
  void didUpdateWidget(Expandable oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (oldWidget.expand == widget.expand) return;

    if (widget.expand) {
      expandController.forward();
    } else {
      expandController.reverse();
    }
  }

  @override
  void dispose() {
    expandController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SizeTransition(axisAlignment: 1.0, sizeFactor: animation, child: widget.child);
  }
}

class ExpandableContent<T> {
  final T data;
  bool _expanded;

  bool get expanded => _expanded;

  ExpandableContent(this.data, this._expanded);

  void expand() {
    _expanded = true;
  }

  void collapse() {
    _expanded = false;
  }

  void toggle() {
    _expanded = !_expanded;
  }
}
