import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:provider/provider.dart';
import 'package:rshb/gateway/product/product.dart';
import 'package:rshb/model/product.dart';
import 'package:rshb/model/response.dart';
import 'package:rshb/storage/favourite/favourite.dart';
import 'package:rshb/util/extensions.dart';
import 'package:rshb/view/base_screen.dart';
import 'package:rshb/view/product_details/product_details_screen.dart';

const double _kScreenPadding = 16;
const double _kGridSpacing = 7;
const double _kGridItemHeight = 307;
const Radius _kRadius8 = Radius.circular(8);
const BorderRadius _kBorderRadius5 = BorderRadius.all(_kRadius8);

final priceSortStrategy = (Product p1, Product p2) => p1.price.compareTo(p2.price);
final ratingSortStrategy = (Product p1, Product p2) => p2.totalRating.compareTo(p1.totalRating);

class ProductListScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Consumer2<ProductGateway, FavouriteProductStorage>(
      builder: (context, gw, fav, child) => BaseScreen(
        child: FutureBuilder<ProductResponse>(
          future: gw.getProductData(),
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return ProductListView(
                data: snapshot.data,
                favourites: fav,
              );
            } else if (snapshot.hasError) {
              print('error while loading data: ${snapshot.error}');
            }
            return Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  SizedBox(
                    child: CircularProgressIndicator(),
                    width: 60,
                    height: 60,
                  ),
                  const Padding(
                    padding: EdgeInsets.only(top: 16),
                    child: Text('Загрузка данных'),
                  )
                ],
              ),
            );
          },
        ),
      ),
    );
  }
}

class ProductListView extends StatefulWidget {
  final ProductResponse data;
  final FavouriteProductStorage favourites;

  const ProductListView({
    Key key,
    this.data,
    this.favourites,
  }) : super(key: key);

  @override
  _ProductListViewState createState() => _ProductListViewState();
}

class _ProductListViewState extends State<ProductListView> {
  bool priceSorted = false;
  int categoryFilterIndex;
  int selectedSectionIndex = 0;
  List<Product> sortedProducts;
  List<CategoryListItem> categoryListItems;

  @override
  void initState() {
    super.initState();
    _setupProducts();
  }

  void _setupProducts() {
    sortedProducts = List<Product>.from(widget.data.products);
    if (categoryFilterIndex != null)
      sortedProducts =
          sortedProducts.where((p) => p.categoryId == widget.data.categoriesMap[categoryFilterIndex].id).toList();

    _sortProducts();
  }

  void _sortProducts() {
    sortedProducts.sort(priceSorted ? priceSortStrategy : ratingSortStrategy);
    _setupCategories();
  }

  void _setupCategories() {
    categoryListItems = [
      CategoryListItem(
        iconPath: 'assets/sort.svg',
        title: 'Сортировать',
        selected: priceSorted,
        onPressed: () => setState(() {
          priceSorted = !priceSorted;
          _sortProducts();
        }),
      ),
      ...widget.data.categories.mapExt((i, e) => CategoryListItem(
            iconPath: 'assets/vegetables.svg',
            title: e.title,
            selected: categoryFilterIndex == i,
            onPressed: () => setState(() {
              categoryFilterIndex = i == categoryFilterIndex ? null : i;
              _setupProducts();
            }),
          ))
    ];
  }

  @override
  void didUpdateWidget(ProductListView oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (oldWidget.data != widget.data) _setupProducts();
  }

  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.of(context).size;

    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: _kScreenPadding),
      child: CustomScrollView(
        slivers: <Widget>[
          _buildCollapsibleToolbar(),
          SliverPadding(
            padding: const EdgeInsets.only(top: 20, bottom: 16),
            sliver: _buildContentGrid(screenSize),
          ),
        ],
      ),
    );
  }

  Widget _buildContentGrid(Size screenSize) {
    if (selectedSectionIndex > 0)
      return SliverFillRemaining(
        child: Center(
          child: Text(
            'TBD',
            style: TextStyle(
              fontSize: 40,
            ),
          ),
        ),
      );

    return SliverGrid(
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        mainAxisSpacing: _kGridSpacing,
        crossAxisSpacing: _kGridSpacing,
        crossAxisCount: 2,
        childAspectRatio: ((screenSize.width - _kScreenPadding * 2 - _kGridSpacing) / 2) / _kGridItemHeight,
      ),
      delegate: SliverChildBuilderDelegate(
        (BuildContext context, int index) {
          final product = sortedProducts[index];

          return _ProductView(
            id: product.id,
            imageUrl: product.image,
            favourite: widget.favourites.favouriteIds.contains(product.id),
            title: product.title,
            unit: product.unit,
            rating: product.totalRating,
            description: product.shortDescription,
            farmer: widget.data.farmersMap[product.farmerId]?.title ?? '-',
            price: product.price,
            ratingCount: product.ratingCount,
            characteristics: product.characteristics.map((e) => Pair(e.title, e.value)).toList(),
            favourites: widget.favourites,
          );
        },
        childCount: sortedProducts.length,
      ),
    );
  }

  SliverAppBar _buildCollapsibleToolbar() {
    return SliverAppBar(
      floating: true,
      leading: Row(
        children: <Widget>[
          Container(
            width: 17,
            height: 14,
            child: SvgPicture.asset('assets/arr_left.svg'),
          ),
        ],
      ),
      titleSpacing: -22,
      title: Text(
        'Каталог',
        style: TextStyle(
          color: Color(0xff1c1c1c),
          fontSize: 20,
          fontFamily: 'SF Pro Display',
          fontWeight: FontWeight.w700,
        ),
      ),
      bottom: PreferredSize(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              height: 50.0,
              padding: const EdgeInsets.all(5),
              decoration: BoxDecoration(
                color: const Color(0xfff5f5f5),
                borderRadius: BorderRadius.circular(50),
              ),
              child: ListView.builder(
                scrollDirection: Axis.horizontal,
                shrinkWrap: true,
                itemCount: widget.data.sections.length,
                itemBuilder: (context, index) {
                  final section = widget.data.sections[index];
                  return GestureDetector(
                    onTap: () => setState(() => selectedSectionIndex = index),
                    child: _NavigationItem(
                      text: section.title,
                      selected: index == selectedSectionIndex,
                    ),
                  );
                },
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 30),
              child: Container(
                height: 76.0,
                child: ListView.builder(
                  scrollDirection: Axis.horizontal,
                  itemCount: categoryListItems.length,
                  itemBuilder: (_, index) => _SortFilterItem(data: categoryListItems[index]),
                ),
              ),
            ),
          ],
        ),
        preferredSize: Size.fromHeight(150),
      ),
      backgroundColor: Colors.white,
      expandedHeight: 240,
    );
  }
}

class _NavigationItem extends StatelessWidget {
  final String text;
  final bool selected;
  final BoxShadow _boxShadow;
  final Color _bgColor;
  final Color _textColor;

  const _NavigationItem({
    Key key,
    @required this.text,
    this.selected = false,
  })  : _boxShadow = selected
            ? const BoxShadow(
                color: Color(0x6642ab44),
                blurRadius: 10,
                offset: Offset(2, 2),
              )
            : null,
        _bgColor = selected ? const Color(0xff41ab43) : Colors.transparent,
        _textColor = selected ? Colors.white : const Color(0xff1c1c1c),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(40),
        boxShadow: [if (_boxShadow != null) _boxShadow],
        color: _bgColor,
      ),
      padding: const EdgeInsets.only(
        top: 11,
        bottom: 12,
      ),
      child: SizedBox(
        width: 111,
        child: Text(
          text,
          textAlign: TextAlign.center,
          style: TextStyle(
            color: _textColor,
            fontSize: 14,
            fontFamily: 'SF Pro Display',
            fontWeight: FontWeight.w600,
          ),
        ),
      ),
    );
  }
}

class _SortFilterItem extends StatelessWidget {
  final CategoryListItem data;

  const _SortFilterItem({
    Key key,
    @required this.data,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: data.onPressed,
      child: Column(
        children: <Widget>[
          Container(
            width: 40,
            height: 40,
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              color: data.selected ? Colors.grey.shade300 : Colors.transparent,
              border: Border.all(
                color: const Color(0xffe0e0e0),
                width: 1,
              ),
            ),
            child: Center(
              child: SvgPicture.asset(
                data.iconPath,
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 5),
            child: ConstrainedBox(
              constraints: const BoxConstraints(
                maxWidth: 90,
              ),
              child: Text(
                data.title,
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: const Color(0xff969696),
                  fontSize: 12,
                  fontFamily: 'SF Pro Display',
                  fontWeight: FontWeight.w500,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class _ProductView extends StatelessWidget {
  final int id;
  final String title;
  final String unit;
  final double rating;
  final int ratingCount;
  final String imageUrl;
  final String description;
  final String farmer;
  final double price;
  final bool favourite;
  final String _heroTag;
  final List<Pair<String, String>> characteristics;
  final FavouriteProductStorage favourites;

  const _ProductView({
    Key key,
    @required this.id,
    @required this.title,
    @required this.unit,
    @required this.rating,
    @required this.ratingCount,
    @required this.imageUrl,
    @required this.description,
    @required this.farmer,
    @required this.price,
    @required this.characteristics,
    @required this.favourite,
    @required this.favourites,
  })  : _heroTag = '$id:$imageUrl',
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.white,
      child: InkWell(
        onTap: () {
          Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => ProductDetailsScreen(
                  id: id,
                  imageUrl: imageUrl,
                  title: title,
                  unit: unit,
                  rating: rating,
                  ratingCount: ratingCount,
                  price: price,
                  description: description,
                  heroTag: _heroTag,
                  characteristics: characteristics,
                ),
              ));
        },
        child: Container(
          decoration: BoxDecoration(
            border: Border.all(
              color: Color(0xffe0e0e0),
            ),
            borderRadius: BorderRadius.circular(8),
          ),
          child: Column(
            children: <Widget>[
              _buildImage(),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(12),
                    child: _buildTitle(),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 12).copyWith(top: 22),
                    child: _buildRating(),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 12).copyWith(top: 8),
                    child: _buildDescription(),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 12).copyWith(top: 8),
                    child: _buildFarmer(),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 12).copyWith(top: 20),
                    child: _buildPrice(),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildPrice() {
    return Text(
      '${price.floor()} ₽',
      style: TextStyle(
        color: Color(0xff1c1c1c),
        fontSize: 14,
        fontFamily: 'SF Pro Display',
        fontWeight: FontWeight.w600,
      ),
    );
  }

  Widget _buildFarmer() {
    return Text(
      farmer,
      style: TextStyle(
        color: Color(0xff1c1c1c),
        fontSize: 10,
        fontFamily: 'SF Pro Display',
        fontWeight: FontWeight.w400,
      ),
    );
  }

  Widget _buildDescription() {
    return Text(
      description,
      maxLines: 2,
      overflow: TextOverflow.ellipsis,
      textAlign: TextAlign.start,
      style: TextStyle(
        color: Color(0xff969696),
        fontSize: 10,
        fontFamily: 'SF Pro Display',
        fontWeight: FontWeight.w400,
      ),
    );
  }

  Widget _buildRating() {
    return Row(
      children: <Widget>[
        Container(
          width: 22,
          height: 22,
          decoration: BoxDecoration(
            color: Color(0xffd7a50c),
            borderRadius: _kBorderRadius5,
          ),
          child: Center(
            child: Text(
              rating.toStringAsFixed(1),
              textAlign: TextAlign.center,
              style: TextStyle(
                color: Color(0xffffffff),
                fontSize: 10,
                fontFamily: 'SF Pro Display',
                fontWeight: FontWeight.w600,
              ),
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 5),
          child: Text(
            '$ratingCount оценок',
            style: TextStyle(
              color: Color(0xff969696),
              fontSize: 10,
              fontFamily: 'SF Pro Display',
              fontWeight: FontWeight.w400,
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildTitle() {
    return RichText(
      text: TextSpan(
        children: <TextSpan>[
          TextSpan(
            text: title,
            style: TextStyle(
              color: Color(0xff1c1c1c),
              fontSize: 12,
              fontFamily: 'SF Pro Display',
              fontWeight: FontWeight.w500,
            ),
          ),
          TextSpan(
            text: ' / $unit',
            style: TextStyle(
              color: Color(0xff969696),
              fontSize: 12,
              fontFamily: 'SF Pro Display',
              fontWeight: FontWeight.w400,
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildImage() {
    return Hero(
      tag: _heroTag,
      child: Container(
        height: 120,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.only(
            topLeft: _kRadius8,
            topRight: _kRadius8,
          ),
          image: DecorationImage(
            fit: BoxFit.cover,
            image: CachedNetworkImageProvider(imageUrl),
          ),
        ),
        child: Padding(
          padding: const EdgeInsets.all(12),
          child: Align(
            alignment: Alignment.topRight,
            child: GestureDetector(
              onTap: () {
                favourites.setFavourite(id, !favourite);
              },
              child: Container(
                width: 32,
                height: 32,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  border: Border.all(
                    color: Color(0xffbdbdbd),
                    style: favourite ? BorderStyle.none : BorderStyle.solid,
                  ),
                  color: Colors.white,
                ),
                child: Center(
                  child: SvgPicture.asset(favourite ? 'assets/favourite_filled.svg' : 'assets/favourite_empty.svg'),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class CategoryListItem {
  final String title;
  final String iconPath;
  final VoidCallback onPressed;
  final bool selected;

  CategoryListItem({
    @required this.title,
    @required this.iconPath,
    @required this.onPressed,
    @required this.selected,
  });
}
