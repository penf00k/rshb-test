# rshb-test

Test task for RSHB bank

[Task](https://docs.google.com/document/d/1yLhaFMSEdxWThxvEqFQgYOjwxgkDHjiMnD9YFwNEg7w/edit)

[Design in Figma](https://www.figma.com/file/yUjRoJi6yzuroTjm4gGtWY/%D0%A2%D0%B5%D1%81%D1%82%D0%BE%D0%B2%D0%BE%D0%B5-%D0%B7%D0%B0%D0%B4%D0%B0%D0%BD%D0%B8%D0%B5?node-id=0%3A1)

[Tasks in Trello](https://trello.com/b/n9OFp4rh/rshb)

To run this project
1. Clone repo and cd to the project dir
2. ``$ flutter pub get``
3. ``$ flutter run``

Spent 10 hours totally

The project uses the latest stable flutter version 1.17.5